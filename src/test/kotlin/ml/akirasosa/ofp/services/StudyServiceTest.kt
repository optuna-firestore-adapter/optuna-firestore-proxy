package ml.akirasosa.ofp.services

import com.google.cloud.Timestamp
import com.google.protobuf.Empty
import com.google.protobuf.Int64Value
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.grpc.StatusRuntimeException
import kotlinx.coroutines.runBlocking
import ml.akirasosa.ofp.*
import ml.akirasosa.ofp.domain.Study
import ml.akirasosa.ofp.domain.Trial
import ml.akirasosa.ofp.repositories.StudyRepository
import ml.akirasosa.ofp.repositories.TrialRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class StudyServiceTest {

    @Test
    fun testGetStudyFromId() = runBlocking {
        val mock = mock<StudyRepository> {
            onBlocking { findById(1) } doReturn Study(study_id = 1)
        }
        val studyService = StudyService(mock, mock())

        val studyMsg = studyService.getStudyFromId(Int64Value.of(1))

        assertEquals(1, studyMsg.studyId)
    }

    @Test
    fun testGetStudyFromIdWhenNotFound() = runBlocking<Unit> {
        val mock = mock<StudyRepository> {
            onBlocking { findById(1) } doReturn null
        }
        val studyService = StudyService(mock, mock())

        assertThrows<StatusRuntimeException> {
            runBlocking {
                studyService.getStudyFromId(Int64Value.of(1))
            }
        }
    }

    @Test
    fun testCreateNewStudyId() = runBlocking {
        val mock = mock<StudyRepository> {
            onBlocking { findByName("my study") } doReturn null
            onBlocking { create("my study") } doReturn Study(study_id = 1)
        }
        val studyService = StudyService(mock, mock())

        val request = CreateNewStudyIdRequest.newBuilder().setStudyName("my study").build()
        val studyMsg = studyService.createNewStudyId(request)

        assertEquals(1, studyMsg.studyId)
    }

    @Test
    fun testCreateNewStudyIdWhenNameExisting() = runBlocking<Unit> {
        val mock = mock<StudyRepository> {
            onBlocking { findByName("my study") } doReturn Study(study_id = 1, study_name = "my study")
        }
        val studyService = StudyService(mock, mock())

        val request = CreateNewStudyIdRequest.newBuilder().setStudyName("my study").build()

        assertThrows<StatusRuntimeException> {
            runBlocking {
                studyService.createNewStudyId(request)
            }
        }
    }

    @Test
    fun testSetStudyUserAttr() = runBlocking {
        val mock = mock<StudyRepository> {
            onBlocking {
                setStudyUserAttr(1, "my key", "my val")
            } doReturn Unit
        }
        val studyService = StudyService(mock, mock())

        val req = SetStudyUserAttrRequest.newBuilder()
            .setStudyId(1)
            .setKey("my key")
            .setValueJson("my val")
            .build()
        val empty = studyService.setStudyUserAttr(req)

        assertEquals(Empty.getDefaultInstance(), empty)
    }

    @Test
    fun testSetStudyDirection() = runBlocking {
        val mock = mock<StudyRepository> {
            onBlocking { findById(1) } doReturn Study(study_id = 1)
        }
        val studyService = StudyService(mock, mock())

        val req = SetStudyDirectionRequest.newBuilder()
            .setStudyId(1)
            .setDirection(StudyDirection.MINIMIZE)
            .build()
        val empty = studyService.setStudyDirection(req)

        assertEquals(Empty.getDefaultInstance(), empty)
    }

    @Test
    fun testSetStudyDirectionWhenProhibited() = runBlocking<Unit> {
        val mock = mock<StudyRepository> {
            onBlocking { findById(1) } doReturn Study(study_id = 1, direction = StudyDirection.MINIMIZE)
        }
        val studyService = StudyService(mock, mock())

        val req = SetStudyDirectionRequest.newBuilder()
            .setStudyId(1)
            .setDirection(StudyDirection.MAXIMIZE)
            .build()

        val e = assertThrows<StatusRuntimeException> {
            runBlocking {
                studyService.setStudyDirection(req)
            }
        }
        assertEquals("FAILED_PRECONDITION: Cannot overwrite study direction from MINIMIZE to MAXIMIZE.", e.message)
    }

    @Test
    fun testSetStudySystemAttr() = runBlocking {
        val mock = mock<StudyRepository> {
            onBlocking {
                setStudySystemAttr(1, "my key", "my val")
            } doReturn Unit
        }
        val studyService = StudyService(mock, mock())

        val req = SetStudySystemAttrRequest.newBuilder()
            .setStudyId(1)
            .setKey("my key")
            .setValueJson("my val")
            .build()
        val empty = studyService.setStudySystemAttr(req)

        assertEquals(Empty.getDefaultInstance(), empty)
    }

    @Test
    fun testGetAllStudySummaries() = runBlocking {
        val studies = listOf(
            Study(study_id = 1),
            Study(study_id = 2)
        )
        val trials = listOf(
            Trial(
                study_id = 2,
                trial_id = 11,
                state = TrialState.COMPLETE,
                value = 0.1,
                datetime_start = Timestamp.now()
            )
        )

        val studyRepositoryMock = mock<StudyRepository> {
            onBlocking { findAll() } doReturn studies
        }
        val trialRepositoryMock = mock<TrialRepository> {
            onBlocking { findAll() } doReturn trials
        }
        val studyService = StudyService(studyRepositoryMock, trialRepositoryMock)

        val response = studyService.getAllStudySummaries(Empty.getDefaultInstance())

        assertEquals(2, response.studySummariesCount)
        assertEquals(1, response.studySummariesList[0].studyId)
        response.studySummariesList[1].let {
            assertEquals(2, it.studyId)
            assertEquals(11, it.bestTrial.trialId)
        }
    }

}