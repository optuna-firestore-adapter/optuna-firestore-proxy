package ml.akirasosa.ofp.services

import com.google.protobuf.BoolValue
import com.google.protobuf.Empty
import com.google.protobuf.Int64Value
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.runBlocking
import ml.akirasosa.ofp.*
import ml.akirasosa.ofp.domain.Trial
import ml.akirasosa.ofp.repositories.TrialCacheRepository
import ml.akirasosa.ofp.repositories.TrialCacheRepositoryImpl
import ml.akirasosa.ofp.repositories.TrialRepository
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.concurrent.ConcurrentHashMap
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class TrialServiceTest {

    private val localStore: ConcurrentHashMap<Long, Trial> = ConcurrentHashMap()
    private val remoteStore: ConcurrentHashMap<Long, Trial> = ConcurrentHashMap()
    private val cacheRepository: TrialCacheRepository = TrialCacheRepositoryImpl(Pair(localStore, remoteStore))

    private val defaultTrial = Trial(trial_id = 1, study_id = 2, state = TrialState.RUNNING)

    @Test
    fun testCreateNewTrialId() = runBlocking {
        val mock = mock<TrialRepository> {
            onBlocking { create(Trial(study_id = 1, state = TrialState.RUNNING)) } doReturn Trial(trial_id = 2)
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = CreateNewTrialIdRequest.newBuilder()
            .setStudyId(1)
            .build()
        val response = trialService.createNewTrialId(request)

        assertEquals(2, response.trialId)
        assertEquals(Trial(trial_id = 2), localStore[2])
    }

    @Test
    fun testGetTrialFromId() = runBlocking {
        localStore[1] = defaultTrial
        val trialService = TrialService(mock(), cacheRepository)

        val request = Int64Value.of(1)
        val response = trialService.getTrialFromId(request)

        assertEquals(1, response.trialId)
    }

    @Test
    fun testSetTrialState() = runBlocking {
        localStore[1] = defaultTrial
        val mock = mock<TrialRepository> {
            onBlocking { setTrialState(1, TrialState.COMPLETE) } doReturn mock()
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = SetTrialStateRequest.newBuilder()
            .setTrialId(1)
            .setState(TrialState.COMPLETE)
            .build()
        val response = trialService.setTrialState(request)

        assertEquals(Empty.getDefaultInstance(), response)
        assertEquals(TrialState.COMPLETE, localStore[1]!!.state)
    }

    @Test
    fun testSetTrialValue() = runBlocking {
        localStore[1] = defaultTrial
        val mock = mock<TrialRepository> {
            onBlocking { setTrialValue(1, 0.1) } doReturn mock()
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = SetTrialValueRequest.newBuilder()
            .setTrialId(1)
            .setValue(0.1)
            .build()
        val response = trialService.setTrialValue(request)

        assertEquals(Empty.getDefaultInstance(), response)
        assertEquals(0.1, localStore[1]!!.value)
    }

    @Test
    fun testSetTrialParam() = runBlocking {
        localStore[1] = defaultTrial
        val mock = mock<TrialRepository> {
            onBlocking {
                setTrialParam(1, "my param", 0.1, "json")
            } doReturn mock()
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = SetTrialParamRequest.newBuilder()
            .setTrialId(1)
            .setParamName("my param")
            .setParamValue(0.1)
            .setDistributionJson("json")
            .build()
        val response = trialService.setTrialParam(request)

        assertEquals(BoolValue.of(true), response)
        assertEquals(
            mapOf(
                "my param" to Trial.TrialParam(0.1, "json")
            ),
            localStore[1]!!.trial_params
        )
    }

    @Test
    fun testSetTrialIntermediateValue() = runBlocking {
        localStore[1] = defaultTrial
        val mock = mock<TrialRepository> {
            onBlocking {
                setTrialIntermediateValue(1, 0, 0.1)
            } doReturn mock()
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = SetTrialIntermediateValueRequest.newBuilder()
            .setTrialId(1)
            .setStep(0)
            .setIntermediateValue(0.1)
            .build()
        val response = trialService.setTrialIntermediateValue(request)

        assertEquals(BoolValue.of(true), response)
        assertEquals(mapOf("0" to 0.1), localStore[1]!!.trial_values)
    }

    @Test
    fun testSetTrialUserAttr() = runBlocking {
        localStore[1] = defaultTrial
        val mock = mock<TrialRepository> {
            onBlocking {
                setStudyUserAttr(1, "my key", "json")
            } doReturn mock()
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = SetTrialUserAttrRequest.newBuilder()
            .setTrialId(1)
            .setKey("my key")
            .setValueJson("json")
            .build()
        val response = trialService.setTrialUserAttr(request)

        assertEquals(Empty.getDefaultInstance(), response)
        assertEquals(mapOf("my key" to "json"), localStore[1]!!.user_attributes)
    }

    @Test
    fun testSetTrialSystemAttr() = runBlocking {
        localStore[1] = defaultTrial
        val mock = mock<TrialRepository> {
            onBlocking {
                setStudySystemAttr(1, "my key", "json")
            } doReturn mock()
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = SetTrialSystemAttrRequest.newBuilder()
            .setTrialId(1)
            .setKey("my key")
            .setValueJson("json")
            .build()
        val response = trialService.setTrialSystemAttr(request)

        assertEquals(Empty.getDefaultInstance(), response)
        assertEquals(mapOf("my key" to "json"), localStore[1]!!.system_attributes)
    }

    @Test
    fun testGetAllTrialsWhenNoLocalCache() = runBlocking {
        val mock = mock<TrialRepository> {
            on { getTrialsObservables(2) } doReturn Observable.empty()
            onBlocking { findAllByStudyId(2) } doReturn listOf(defaultTrial)
        }
        val trialService = TrialService(mock, cacheRepository)

        val request = GetAllTrialsRequest.newBuilder()
            .setStudyId(2)
            .build()
        val response = trialService.getAllTrials(request)

        assertEquals(1, response.trialsCount)
    }

    @Test
    fun testGetAllTrialsWhenLocalCacheExists() = runBlocking<Unit> {
        val subject = PublishSubject.create<Trial>()

        val mock = mock<TrialRepository> {
            on { getTrialsObservables(2) } doReturn subject
            onBlocking { findAllByStudyId(2) } doReturn listOf()
        }
        val trialService = TrialService(mock, cacheRepository)

        // Once send request to start observing
        val request = GetAllTrialsRequest.newBuilder()
            .setStudyId(2)
            .build()
        val response = trialService.getAllTrials(request)

        // Confirm empty
        assertEquals(0, response.trialsCount)
        assertEquals(0, remoteStore.size)

        // Confirm observing
        suspendCoroutine<Boolean> { cont ->
            subject.subscribe {
                assertEquals(1, remoteStore.size)
                cont.resume(true)
            }
            subject.onNext(defaultTrial)
        }

        // Request again
        val response2 = trialService.getAllTrials(request)
        assertEquals(1, response2.trialsCount)
    }

    @AfterEach
    fun clearStore() {
        localStore.clear()
        remoteStore.clear()
    }

}