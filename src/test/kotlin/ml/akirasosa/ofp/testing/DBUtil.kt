package ml.akirasosa.ofp.testing

import com.google.cloud.firestore.CollectionReference


fun deleteCollection(collection: CollectionReference, batchSize: Int = 1) {
    try {
        // retrieve a small batch of documents to avoid out-of-memory errors
        val future = collection.limit(batchSize).get()
        var deleted = 0
        // future.get() blocks on document retrieval
        val documents = future.get().documents
        for (document in documents) {
            document.reference.delete()
            ++deleted
        }
        if (deleted >= batchSize) {
            // retrieve and delete another batch
            deleteCollection(collection, batchSize)
        }
    } catch (e: Exception) {
        System.err.println("Error deleting collection : " + e.message)
    }

}