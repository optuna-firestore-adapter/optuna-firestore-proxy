package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.Firestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import ml.akirasosa.ofp.StudyDirection
import ml.akirasosa.ofp.domain.Study
import ml.akirasosa.ofp.repositories.RepositoryBaseTest.CollectionPath.STUDIES
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.kodein.di.generic.instance
import java.util.*

class StudyRepositoryTest : RepositoryBaseTest(), FirestoreRepository {

    private val db: Firestore by instance()
    private val repo: StudyRepository by instance()

    @Test
    fun testCreate() = runBlocking {
        val study = repo.create("my study")

        assertEquals(1L, study.study_id)
        assertEquals("my study", study.study_name)
    }

    @Test
    fun testCreateWhenBlankNameIsGiven() = runBlocking {
        val study = repo.create("")

        assertDoesNotThrow {
            UUID.fromString(study.study_name)
        }
    }

    @Test
    fun testFindById() = runBlocking {
        val study = Study()
        db.collection(STUDIES).document("1").set(study).await()

        val studyFound = repo.findById(1)

        assertEquals(study, studyFound)
    }

    @Test
    fun testFindByIdWhenNoDocument() = runBlocking {
        val studyFound = repo.findById(1)

        assertNull(studyFound)
    }

    @Test
    fun testFindByName() = runBlocking {
        val study = Study(study_name = "my name")
        db.collection(STUDIES).document("1").set(study).await()

        val studyFound = repo.findByName("my name")

        assertEquals(study, studyFound)
    }

    @Test
    fun testFindByNameWhenNoDocument() = runBlocking {
        val studyFound = repo.findByName("my name")

        assertNull(studyFound)
    }

    @Test
    fun testFindAll() = runBlocking {
        val studies = listOf(
            Study(study_id = 1, study_name = "name1"),
            Study(study_id = 2, study_name = "name2")
        )
        studies.forEach {
            launch {
                db.collection(STUDIES)
                    .document(it.study_id.toString())
                    .set(it)
                    .await()
            }.join()
        }

        val studiesFound = repo.findAll()

        assertEquals(studies.toSet(), studiesFound.toSet())
    }

    @Test
    fun testSetStudyUserAttr() = runBlocking {
        val study = Study()
        db.collection(STUDIES).document("1").set(study).await()

        repo.setStudyUserAttr(1, "my key", "my val")
        repo.setStudyUserAttr(1, "my key2", "my val2")

        val snapshot = db.collection(STUDIES).document("1").get().await()

        assertEquals(
            snapshot.data!!["user_attributes"], mapOf(
                "my key" to "my val",
                "my key2" to "my val2"
            )
        )
    }

    @Test
    fun testSetStudyDirection() = runBlocking {
        val study = Study()
        db.collection(STUDIES).document("1").set(study).await()

        repo.setStudyDirection(1, StudyDirection.MINIMIZE)

        val snapshot = db.collection(STUDIES).document("1").get().await()

        assertEquals(snapshot.data!!["direction"], "MINIMIZE")
    }

    @Test
    fun testSetStudySystemAttr() = runBlocking {
        val study = Study()
        db.collection(STUDIES).document("1").set(study).await()

        repo.setStudySystemAttr(1, "my key", "my val")
        repo.setStudySystemAttr(1, "my key2", "my val2")

        val snapshot = db.collection(STUDIES).document("1").get().await()

        assertEquals(
            snapshot.data!!["system_attributes"], mapOf(
                "my key" to "my val",
                "my key2" to "my val2"
            )
        )
    }

}