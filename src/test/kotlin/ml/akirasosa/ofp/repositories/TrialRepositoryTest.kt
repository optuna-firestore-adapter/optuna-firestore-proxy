package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.Firestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import ml.akirasosa.ofp.TrialState
import ml.akirasosa.ofp.domain.Trial
import ml.akirasosa.ofp.repositories.RepositoryBaseTest.CollectionPath.TRIALS
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.kodein.di.generic.instance
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class TrialRepositoryTest : RepositoryBaseTest(), FirestoreRepository {

    private val db: Firestore by instance()
    private val repo: TrialRepository by instance()

    @Test
    fun testCreate() = runBlocking {
        val createdTrial = repo.create(Trial())

        assertEquals(1L, createdTrial.trial_id)

        // Retrieve and check datetime_start
        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertNotNull(snapshot.data!!["datetime_start"])
    }

    @Test
    fun testFindAllByStudyId() = runBlocking {
        val trials = listOf(
            Trial(trial_id = 1, study_id = 11),
            Trial(trial_id = 2, study_id = 11),
            Trial(trial_id = 3, study_id = 12)
        )
        trials.forEach {
            launch {
                db.collection(TRIALS)
                    .document(it.trial_id.toString())
                    .set(it)
                    .await()
            }.join()
        }

        val trialsFound = repo.findAllByStudyId(studyId = 11)

        assertEquals(trials.slice(0..1).toSet(), trialsFound.toSet())
    }

    @Test
    fun testFindAll() = runBlocking {
        val trials = listOf(
            Trial(trial_id = 1),
            Trial(trial_id = 2)
        )
        trials.forEach {
            launch {
                db.collection(TRIALS)
                    .document(it.trial_id.toString())
                    .set(it)
                    .await()
            }.join()
        }

        val trialsFound = repo.findAll()

        assertEquals(trials.slice(0..1).toSet(), trialsFound.toSet())
    }

    @Test
    fun testSetTrialStateWhenTrialFinished() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setTrialState(1, TrialState.PRUNED)

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(snapshot.data!!["state"], "PRUNED")
        assertNotNull(snapshot.data!!["datetime_complete"])
    }

    @Test
    fun testSetTrialStateWhenTrialNotFinished() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setTrialState(1, TrialState.FAIL)

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(snapshot.data!!["state"], "FAIL")
        assertNull(snapshot.data!!["datetime_complete"])
    }

    @Test
    fun testSetTrialValue() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setTrialValue(1, 0.1)

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(snapshot.data!!["value"], 0.1)
    }

    @Test
    fun testSetTrialParam() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setTrialParam(1, "my param", 0.1, "encoded distribution1")
        repo.setTrialParam(1, "my param2", 0.2, "encoded distribution2")

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(
            snapshot.data!!["trial_params"], mapOf(
                "my param" to mapOf(
                    "param_value" to 0.1,
                    "distribution_json" to "encoded distribution1"
                ),
                "my param2" to mapOf(
                    "param_value" to 0.2,
                    "distribution_json" to "encoded distribution2"
                )
            )
        )
    }

    @Test
    fun testSetTrialIntermediateValue() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setTrialIntermediateValue(1, 0, 0.1)
        repo.setTrialIntermediateValue(1, 1, 0.2)

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(
            snapshot.data!!["trial_values"], mapOf(
                "0" to 0.1,
                "1" to 0.2
            )
        )
    }

    @Test
    fun testSetStudyUserAttr() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setStudyUserAttr(1, "my attr", "my val")
        repo.setStudyUserAttr(1, "my attr2", "my val2")

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(
            snapshot.data!!["user_attributes"], mapOf(
                "my attr" to "my val",
                "my attr2" to "my val2"
            )
        )
    }

    @Test
    fun testSetStudySystemAttr() = runBlocking {
        val trial = Trial()
        db.collection(TRIALS).document("1").set(trial).await()

        repo.setStudySystemAttr(1, "my attr", "my val")
        repo.setStudySystemAttr(1, "my attr2", "my val2")

        val snapshot = db.collection(TRIALS).document("1").get().await()

        assertEquals(
            snapshot.data!!["system_attributes"], mapOf(
                "my attr" to "my val",
                "my attr2" to "my val2"
            )
        )
    }

    @Test
    fun testGetTrialsObservables() = runBlocking {
        val observable = repo.getTrialsObservables(1)
        val trialToBeCreated = Trial(study_id = 1)
        val actualTrial = suspendCoroutine<Trial> { cont ->
            observable.subscribe {
                cont.resume(it)
            }
            db.collection(TRIALS).document("11").set(trialToBeCreated)
        }

        assertEquals(trialToBeCreated, actualTrial)
    }

}