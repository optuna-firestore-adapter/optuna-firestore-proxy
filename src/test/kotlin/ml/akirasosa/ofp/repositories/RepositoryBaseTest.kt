package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.Firestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import ml.akirasosa.ofp.repositories.RepositoryBaseTest.CollectionPath.SEQ
import ml.akirasosa.ofp.repositories.RepositoryBaseTest.CollectionPath.STUDIES
import ml.akirasosa.ofp.repositories.RepositoryBaseTest.CollectionPath.TRIALS
import ml.akirasosa.ofp.testing.deleteCollection
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.TestInstance
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class RepositoryBaseTest : KodeinAware {

    object CollectionPath {
        const val SEQ = "test_optuna_seq"
        const val STUDIES = "test_optuna_studies"
        const val TRIALS = "test_optuna_trials"
    }

    override val kodein = Kodein {
        import(repositoryModule, allowOverride = true)
        bind<SequenceRepository>(overrides = true) with provider {
            SequenceRepositoryImpl(instance(), SEQ)
        }
        bind<StudyRepository>(overrides = true) with provider {
            StudyRepositoryImpl(instance(), instance(), STUDIES)
        }
        bind<TrialRepository>(overrides = true) with provider {
            TrialRepositoryImpl(instance(), instance(), TRIALS)
        }
    }

    private val db: Firestore by instance()

    @AfterEach
    fun clearCollections() = runBlocking {
        listOf(SEQ, STUDIES, TRIALS).forEach {
            launch {
                deleteCollection(db.collection(it))
            }.join()
        }
    }

}