package ml.akirasosa.ofp.repositories

import com.google.cloud.Timestamp
import ml.akirasosa.ofp.TrialState
import ml.akirasosa.ofp.domain.Trial
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.concurrent.ConcurrentHashMap

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TrialCacheRepositoryTest {

    private val localStore: ConcurrentHashMap<Long, Trial> = ConcurrentHashMap()
    private val remoteStore: ConcurrentHashMap<Long, Trial> = ConcurrentHashMap()
    private val repo = TrialCacheRepositoryImpl(Pair(localStore, remoteStore))

    @Test
    fun testGet() {
        localStore[1] = Trial(trial_id = 1)
        remoteStore[2] = Trial(trial_id = 2)

        assertNull(repo.get(0))
        assertEquals(Trial(trial_id = 1), repo.get(1))
        assertEquals(Trial(trial_id = 2), repo.get(2))
    }

    @Test
    fun testGelAllTrials() {
        localStore[1] = Trial(trial_id = 1, study_id = 11)
        localStore[2] = Trial(trial_id = 2, study_id = 12)
        remoteStore[3] = Trial(trial_id = 3, study_id = 11)
        remoteStore[4] = Trial(trial_id = 4, study_id = 12)

        val trialsGot = repo.gelAllTrials(11)

        assertEquals(
            setOf(
                Trial(trial_id = 1, study_id = 11),
                Trial(trial_id = 3, study_id = 11)
            ),
            trialsGot
        )
    }

    @Test
    fun testPutAsLocal() {
        val trial = Trial(trial_id = 1)

        repo.putAsLocal(trial)

        assertEquals(trial, localStore[1])
        assertTrue(remoteStore.isEmpty())
    }

    @Test
    fun testPutAsRemote() {
        val trial = Trial(trial_id = 1)

        repo.putAsRemote(trial)

        assertEquals(trial, remoteStore[1])
        assertTrue(localStore.isEmpty())
    }

    @Test
    fun testPutAsRemoteWhenExistInLocal() {
        val trial = Trial(trial_id = 1)
        localStore[1] = trial

        val trialRemote = Trial(
            trial_id = 1,
            datetime_start = Timestamp.now(),
            datetime_complete = Timestamp.now()
        )
        repo.putAsRemote(trialRemote)

        assertEquals(trialRemote.datetime_start, localStore[1]!!.datetime_start)
        assertEquals(trialRemote.datetime_complete, localStore[1]!!.datetime_complete)
        assertTrue(remoteStore.isEmpty())
    }

    @Test
    fun testSetTrialState() {
        localStore[1] = Trial()

        repo.setTrialState(1, TrialState.COMPLETE)

        assertEquals(TrialState.COMPLETE, localStore[1]!!.state)
    }

    @Test
    fun testSetTrialValue() {
        localStore[1] = Trial()

        repo.setTrialValue(1, 0.1)

        assertEquals(0.1, localStore[1]!!.value)
    }

    @Test
    fun testSetTrialParam() {
        localStore[1] = Trial(
            trial_params = mapOf(
                "my param1" to Trial.TrialParam(0.1, "encoded json1")
            )
        )

        repo.setTrialParam(1, "my param2", 0.2, "encoded json2")

        assertEquals(
            mapOf(
                "my param1" to Trial.TrialParam(0.1, "encoded json1"),
                "my param2" to Trial.TrialParam(0.2, "encoded json2")
            ),
            localStore[1]!!.trial_params
        )
    }

    @Test
    fun testSetIntermediateValue() {
        localStore[1] = Trial(
            trial_values = mapOf("0" to 0.1)
        )

        repo.setTrialIntermediateValue(1, 1, 0.2)

        assertEquals(
            mapOf(
                "0" to 0.1,
                "1" to 0.2
            ),
            localStore[1]!!.trial_values
        )
    }

    @Test
    fun testSetStudyUserAttr() {
        localStore[1] = Trial(
            user_attributes = mapOf("my key1" to "my val1")
        )

        repo.setStudyUserAttr(1, "my key2", "my val2")

        assertEquals(
            mapOf(
                "my key1" to "my val1",
                "my key2" to "my val2"
            ),
            localStore[1]!!.user_attributes
        )
    }

    @Test
    fun testSetStudySystemAttr() {
        localStore[1] = Trial(
            system_attributes = mapOf("my key1" to "my val1")
        )

        repo.setStudySystemAttr(1, "my key2", "my val2")

        assertEquals(
            mapOf(
                "my key1" to "my val1",
                "my key2" to "my val2"
            ),
            localStore[1]!!.system_attributes
        )
    }

    @AfterEach
    fun clearStore() {
        localStore.clear()
        remoteStore.clear()
    }

}