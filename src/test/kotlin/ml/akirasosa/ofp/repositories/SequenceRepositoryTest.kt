package ml.akirasosa.ofp.repositories

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.kodein.di.generic.instance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SequenceRepositoryTest : RepositoryBaseTest() {

    private val repo: SequenceRepository by instance()

    @Test
    fun testGetSequenceOf() = runBlocking {
        val seq1 = repo.getSequenceOf("my_collection")
        val seq2 = repo.getSequenceOf("my_collection")
        assertEquals(1, seq1)
        assertEquals(2, seq2)
    }

}