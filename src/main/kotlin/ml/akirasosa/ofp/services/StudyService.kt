package ml.akirasosa.ofp.services

import com.google.protobuf.Empty
import com.google.protobuf.Int64Value
import com.google.protobuf.StringValue
import com.google.protobuf.Timestamp
import io.grpc.Status
import ml.akirasosa.ofp.*
import ml.akirasosa.ofp.repositories.StudyRepository
import ml.akirasosa.ofp.repositories.TrialRepository

class StudyService(
    private val studyRepository: StudyRepository,
    private val trialRepository: TrialRepository
) : StudyServiceCoroutineGrpc.StudyServiceImplBase(), RpcService {

    override suspend fun getStudyFromId(request: Int64Value): StudyMsg {
        val studyId = request.value

        return studyRepository.findById(studyId)
            ?.toMessage()
            ?: throw Status.NOT_FOUND.asRuntimeException()
    }

    override suspend fun getStudyFromName(request: StringValue): StudyMsg {
        val studyName = request.value

        return studyRepository.findByName(studyName)
            ?.toMessage()
            ?: throw Status.NOT_FOUND.asRuntimeException()
    }

    override suspend fun createNewStudyId(
        request: CreateNewStudyIdRequest
    ): StudyMsg {
        val study = studyRepository.findByName(request.studyName)

        if (study != null) {
            throw Status.ALREADY_EXISTS
                .withDescription("Study name ${study.study_name} already exists.")
                .asRuntimeException()
        }

        val newStudy = studyRepository.create(request.studyName)

        return newStudy.toMessage()
    }

    override suspend fun setStudyUserAttr(request: SetStudyUserAttrRequest): Empty {
        val studyId = request.studyId
        val key = request.key
        val valueJson = request.valueJson

        studyRepository.setStudyUserAttr(studyId, key, valueJson)

        return Empty.getDefaultInstance()
    }

    override suspend fun setStudyDirection(request: SetStudyDirectionRequest): Empty {
        val studyId = request.studyId
        val direction = request.direction
        val study = studyRepository.findById(studyId) ?: throw Status.NOT_FOUND.asRuntimeException()

        if (study.direction != StudyDirection.NOT_SET && study.direction != direction) {
            throw  Status.FAILED_PRECONDITION
                .withDescription("Cannot overwrite study direction from ${study.direction} to $direction.")
                .asRuntimeException()
        }

        studyRepository.setStudyDirection(studyId, direction)

        return Empty.getDefaultInstance()
    }

    override suspend fun setStudySystemAttr(request: SetStudySystemAttrRequest): Empty {
        val studyId = request.studyId
        val key = request.key
        val valueJson = request.valueJson

        studyRepository.setStudySystemAttr(studyId, key, valueJson)

        return Empty.getDefaultInstance()
    }

    override suspend fun getAllStudySummaries(request: Empty): GetAllStudySummariesResponse {
        val studies = studyRepository.findAll()
        val trials = trialRepository.findAll()

        val msgList = studies.map { s ->
            val studyTrials = trials
                .filter { it.study_id == s.study_id }
            val bestTrial = studyTrials
                .filter { it.state == TrialState.COMPLETE }
                .minBy { it.value!! }
            val datetimeStart = studyTrials
                .map { it.datetime_start }
                .minBy { it!! }

            StudySummaryMsg.newBuilder()
                .setStudyId(s.study_id!!)
                .setStudyName(s.study_name)
                .setDirection(s.direction)
                .putAllUserAttributes(s.user_attributes)
                .putAllSystemAttributes(s.system_attributes)
                .setNTrials(trials.size)
                .apply {
                    bestTrial?.let {
                        setBestTrial(it.toMessage())
                    }
                    datetimeStart?.let {
                        setDatetimeStart(
                            Timestamp.newBuilder()
                                .setSeconds(it.seconds)
                                .setNanos(it.nanos)
                        )
                    }
                }
                .build()
        }

        return GetAllStudySummariesResponse.newBuilder()
            .addAllStudySummaries(msgList)
            .build()
    }

}
