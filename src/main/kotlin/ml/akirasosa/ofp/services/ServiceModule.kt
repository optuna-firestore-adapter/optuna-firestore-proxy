package ml.akirasosa.ofp.services

import ml.akirasosa.ofp.StudyServiceCoroutineGrpc
import ml.akirasosa.ofp.TrialServiceCoroutineGrpc
import ml.akirasosa.ofp.TrialServiceGrpc
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

val serviceModule = Kodein.Module("serviceModule") {
    bind<StudyServiceCoroutineGrpc.StudyServiceImplBase>() with provider {
        StudyService(instance(), instance())
    }
    bind<TrialServiceCoroutineGrpc.TrialServiceImplBase>() with provider {
        TrialService(instance(), instance())
    }
}