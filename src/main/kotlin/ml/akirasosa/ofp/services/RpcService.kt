package ml.akirasosa.ofp.services

import com.google.protobuf.Timestamp
import ml.akirasosa.ofp.StudyMsg
import ml.akirasosa.ofp.TrialMsg
import ml.akirasosa.ofp.TrialParamMsg
import ml.akirasosa.ofp.domain.Study
import ml.akirasosa.ofp.domain.Trial

interface RpcService {

     fun Trial.toMessage(): TrialMsg {
        return TrialMsg.newBuilder()
            .setTrialId(trial_id!!)
            .setStudyId(study_id!!)
            .setState(state)
            .putAllUserAttributes(user_attributes)
            .apply {
                trial_params.forEach {
                    val value = TrialParamMsg.newBuilder()
                        .setParamValue(it.value.param_value!!)
                        .setDistributionJson(it.value.distribution_json)
                        .setIsExist(true)
                        .build()
                    putTrialParams(it.key, value)
                }
                trial_values.forEach {
                    putTrialValues(it.key.toInt(), it.value)
                }
                datetime_start?.let {
                    setDatetimeStart(
                        Timestamp.newBuilder()
                            .setSeconds(it.seconds)
                            .setNanos(it.nanos)
                    )
                }
                datetime_complete?.let {
                    setDatetimeComplete(
                        Timestamp.newBuilder()
                            .setSeconds(it.seconds)
                            .setNanos(it.nanos)
                    )
                }
                this@toMessage.value?.let(this::setValue)
            }
            .build()
    }

    fun Study.toMessage(): StudyMsg {
        return StudyMsg.newBuilder()
            .setStudyId(study_id!!)
            .setStudyName(study_name)
            .setDirection(direction)
            .putAllUserAttributes(user_attributes)
            .putAllSystemAttributes(system_attributes)
            .build()
    }
}
