package ml.akirasosa.ofp.services

import com.google.protobuf.BoolValue
import com.google.protobuf.Empty
import com.google.protobuf.Int64Value
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.launch
import ml.akirasosa.ofp.*
import ml.akirasosa.ofp.domain.Trial
import ml.akirasosa.ofp.repositories.TrialCacheRepository
import ml.akirasosa.ofp.repositories.TrialRepository
import org.slf4j.LoggerFactory


class TrialService(
    private val trialRepository: TrialRepository,
    private val trialCacheRepository: TrialCacheRepository
) : TrialServiceCoroutineGrpc.TrialServiceImplBase(), RpcService {

    private val logger = LoggerFactory.getLogger(TrialService::class.java.name)
    private val disposables = mutableMapOf<Long, Disposable>()

    override suspend fun createNewTrialId(request: CreateNewTrialIdRequest): CreateNewTrialIdResponse {
        val studyId = request.studyId
        val trial = Trial(study_id = studyId, state = TrialState.RUNNING)

        val newTrial = trialRepository.create(trial)
        trialCacheRepository.putAsLocal(newTrial)

        return CreateNewTrialIdResponse.newBuilder()
            .setTrialId(newTrial.trial_id!!)
            .build()
    }

    override suspend fun getAllTrials(request: GetAllTrialsRequest): GetAllTrialsResponse {
        val studyId = request.studyId

        if (!disposables.containsKey(studyId)) {
            logger.info("Start observing trials having study_id $studyId.")
            val disposable = trialRepository.getTrialsObservables(studyId)
                .subscribe {
                    trialCacheRepository.putAsRemote(it)
                }
            disposables[studyId] = disposable
        }

        val cachedTrials = trialCacheRepository.gelAllTrials(studyId)
        if (cachedTrials.isNotEmpty()) {
//            logger.debug("getAllTrials from cache")
            return GetAllTrialsResponse.newBuilder()
                .addAllTrials(cachedTrials.map { it.toMessage() })
                .build()
        }

        val trials = trialRepository.findAllByStudyId(studyId)
        return GetAllTrialsResponse.newBuilder()
            .addAllTrials(trials.map { it.toMessage() })
            .build()
    }

    override suspend fun getTrialFromId(request: Int64Value): TrialMsg {
        val trialId = request.value
        val trial = trialCacheRepository.get(trialId)!!
        return trial.toMessage()
    }

    override suspend fun setTrialState(request: SetTrialStateRequest): Empty {
        val trialId = request.trialId
        val state = request.state

        trialCacheRepository.setTrialState(trialId, state)
        launch { trialRepository.setTrialState(trialId, state) }

        return Empty.getDefaultInstance()
    }

    override suspend fun setTrialValue(request: SetTrialValueRequest): Empty {
        val trialId = request.trialId
        val value = request.value

        trialCacheRepository.setTrialValue(trialId, value)
        launch { trialRepository.setTrialValue(trialId, value) }

        return Empty.getDefaultInstance()
    }

    override suspend fun setTrialParam(request: SetTrialParamRequest): BoolValue {
        val trialId = request.trialId
        val paramName = request.paramName
        val paramValue = request.paramValue
        val distributionJson = request.distributionJson

        val trial = trialCacheRepository.get(trialId)!!
        if (trial.trial_params.containsKey(paramName)) {
            return BoolValue.newBuilder().setValue(false).build()
        }

        trialCacheRepository.setTrialParam(trialId, paramName, paramValue, distributionJson)
        launch { trialRepository.setTrialParam(trialId, paramName, paramValue, distributionJson) }

        return BoolValue.newBuilder().setValue(true).build()
    }

    override suspend fun setTrialIntermediateValue(request: SetTrialIntermediateValueRequest): BoolValue {
        val trialId = request.trialId
        val step = request.step
        val intermediateValue = request.intermediateValue

        val trial = trialCacheRepository.get(trialId)!!
        if (trial.trial_values.containsKey(step.toString())) {
            return BoolValue.newBuilder().setValue(false).build()
        }

        trialCacheRepository.setTrialIntermediateValue(trialId, step, intermediateValue)
        launch { trialRepository.setTrialIntermediateValue(trialId, step, intermediateValue) }

        return BoolValue.newBuilder().setValue(true).build()
    }

    override suspend fun setTrialUserAttr(request: SetTrialUserAttrRequest): Empty {
        val trialId = request.trialId
        val key = request.key
        val valueJson = request.valueJson

        trialCacheRepository.setStudyUserAttr(trialId, key, valueJson)
        launch { trialRepository.setStudyUserAttr(trialId, key, valueJson) }

        return Empty.getDefaultInstance()
    }

    override suspend fun setTrialSystemAttr(request: SetTrialSystemAttrRequest): Empty {
        val trialId = request.trialId
        val key = request.key
        val valueJson = request.valueJson

        trialCacheRepository.setStudySystemAttr(trialId, key, valueJson)
        launch { trialRepository.setStudySystemAttr(trialId, key, valueJson) }

        return Empty.getDefaultInstance()
    }

}



