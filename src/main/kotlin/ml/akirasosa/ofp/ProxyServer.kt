package ml.akirasosa.ofp

import io.grpc.Server
import io.grpc.ServerBuilder
import ml.akirasosa.ofp.repositories.repositoryModule
import ml.akirasosa.ofp.services.serviceModule
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import org.slf4j.LoggerFactory
import java.io.IOException


class ProxyServer : KodeinAware {
    private var server: Server? = null

    override val kodein = Kodein {
        import(repositoryModule)
        import(serviceModule)
    }

    private val studyService: StudyServiceCoroutineGrpc.StudyServiceImplBase by instance()
    private val trialService: TrialServiceCoroutineGrpc.TrialServiceImplBase by instance()
    private val logger = LoggerFactory.getLogger(ProxyServer::class.java.name)

    @Throws(IOException::class)
    private fun start() {
        val port = 50051

        server = ServerBuilder.forPort(port)
            .addService(studyService)
            .addService(trialService)
            .build()
            .start()
        logger.info("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(object : Thread() {
            override fun run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down")
                this@ProxyServer.stop()
                System.err.println("*** server shut down")
            }
        })
    }

    private fun stop() {
        server?.shutdown()
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    @Throws(InterruptedException::class)
    private fun blockUntilShutdown() {
        server?.awaitTermination()
    }

    companion object {
        /**
         * Main launches the server from the command line.
         */
        @Throws(IOException::class, InterruptedException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val server = ProxyServer()
            server.start()
            server.blockUntilShutdown()
        }
    }
}