package ml.akirasosa.ofp.domain

import ml.akirasosa.ofp.StudyDirection


data class Study(
    val study_id: Long? = null,
    val study_name: String = "",
    val user_attributes: Map<String, String> = mapOf(),
    val system_attributes: Map<String, String> = mapOf(),
    val direction: StudyDirection = StudyDirection.NOT_SET
)