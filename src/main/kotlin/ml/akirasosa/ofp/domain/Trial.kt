package ml.akirasosa.ofp.domain

import com.google.cloud.Timestamp
import ml.akirasosa.ofp.TrialState

data class Trial(
    val trial_id: Long? = null,
    val study_id: Long? = null,
    val state: TrialState? = null,
    val datetime_start: Timestamp? = null,
    val datetime_complete: Timestamp? = null,
    val value: Double? = null,
    val trial_params: Map<String, TrialParam> = mapOf(),
    val trial_values: Map<String, Double> = mapOf(),
    val user_attributes: Map<String, String> = mapOf(),
    val system_attributes: Map<String, String> = mapOf()
) {
    data class TrialParam(
        val param_value: Double? = null,
        val distribution_json: String? = ""
    )
}