package ml.akirasosa.ofp.repositories

import ml.akirasosa.ofp.TrialState
import ml.akirasosa.ofp.domain.Trial

interface TrialCacheRepository {
    fun gelAllTrials(studyId: Long): Set<Trial>
    fun putAsLocal(trial: Trial)
    fun putAsRemote(trial: Trial)
    fun setTrialState(trialId: Long, state: TrialState)
    fun setTrialValue(trialId: Long, value: Double)
    fun setTrialParam(trialId: Long, paramName: String, paramValue: Double, distributionJson: String)
    fun setTrialIntermediateValue(trialId: Long, step: Int, intermediateValue: Double)
    fun setStudyUserAttr(trialId: Long, key: String, valueJson: String)
    fun setStudySystemAttr(trialId: Long, key: String, valueJson: String)
    fun get(trialId: Long): Trial?
}