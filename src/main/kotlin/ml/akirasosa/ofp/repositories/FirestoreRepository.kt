package ml.akirasosa.ofp.repositories

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.concurrent.Future

interface FirestoreRepository {
    suspend fun <V> Future<V>.await(): V {
        return GlobalScope.async { get() }.await()
    }
}
