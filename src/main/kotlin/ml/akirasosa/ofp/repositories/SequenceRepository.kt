package ml.akirasosa.ofp.repositories

interface SequenceRepository : FirestoreRepository {
    suspend fun getSequenceOf(name: String): Long
}