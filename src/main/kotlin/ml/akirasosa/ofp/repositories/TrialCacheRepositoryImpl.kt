package ml.akirasosa.ofp.repositories

import com.google.common.collect.ImmutableMap
import ml.akirasosa.ofp.TrialState
import ml.akirasosa.ofp.domain.Trial
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap

class TrialCacheRepositoryImpl(trialStore: TrialStore) : TrialCacheRepository {

    private val localStore: ConcurrentHashMap<Long, Trial> = trialStore.first
    private val remoteStore: ConcurrentHashMap<Long, Trial> = trialStore.second

    @Suppress("unused")
    private val logger = LoggerFactory.getLogger(TrialCacheRepositoryImpl::class.java.name)

    override fun get(trialId: Long): Trial? {
        if (localStore.containsKey(trialId)) return localStore[trialId]
        if (remoteStore.containsKey(trialId)) return remoteStore[trialId]
        return null
    }

    override fun gelAllTrials(studyId: Long): Set<Trial> {
        val local = localStore.values.filter { it.study_id == studyId }
        val remote = remoteStore.values.filter { it.study_id == studyId }

        return local.union(remote)
    }

    override fun putAsLocal(trial: Trial) {
        localStore[trial.trial_id!!] = trial
    }

    override fun putAsRemote(trial: Trial) {
        // It's already managed in localStore.
        if (localStore.containsKey(trial.trial_id)) {
            localStore.computeIfPresent(trial.trial_id!!) { _, v ->
                v.copy(
                    datetime_start = v.datetime_start ?: trial.datetime_start,
                    datetime_complete = v.datetime_complete ?: trial.datetime_complete
                )
            }
            return
        }
        remoteStore[trial.trial_id!!] = trial
    }

    override fun setTrialState(trialId: Long, state: TrialState) {
        localStore.compute(trialId) { _, v ->
            v!!.copy(state = state)
        }
    }

    override fun setTrialValue(trialId: Long, value: Double) {
        localStore.compute(trialId) { _, v ->
            v!!.copy(value = value)
        }
    }

    override fun setTrialParam(trialId: Long, paramName: String, paramValue: Double, distributionJson: String) {
        localStore.compute(trialId) { _, v ->
            val trialParam = Trial.TrialParam(
                param_value = paramValue,
                distribution_json = distributionJson
            )
            val newTrialParams = ImmutableMap.builder<String, Trial.TrialParam>()
                .putAll(v!!.trial_params.filterKeys { k -> k != paramName })
                .put(paramName, trialParam)
                .build()
            v.copy(trial_params = newTrialParams)
        }
    }

    override fun setTrialIntermediateValue(trialId: Long, step: Int, intermediateValue: Double) {
        localStore.compute(trialId) { _, v ->
            val newTrialValues = ImmutableMap.builder<String, Double>()
                .putAll(v!!.trial_values.filterKeys { k -> k != step.toString() })
                .put(step.toString(), intermediateValue)
                .build()
            v.copy(trial_values = newTrialValues)
        }
    }

    override fun setStudyUserAttr(trialId: Long, key: String, valueJson: String) {
        localStore.compute(trialId) { _, v ->
            val attrs = ImmutableMap.builder<String, String>()
                .putAll(v!!.user_attributes.filterKeys { k -> k != key })
                .put(key, valueJson)
                .build()
            v.copy(user_attributes = attrs)
        }
    }

    override fun setStudySystemAttr(trialId: Long, key: String, valueJson: String) {
        localStore.compute(trialId) { _, v ->
            val attrs = ImmutableMap.builder<String, String>()
                .putAll(v!!.system_attributes.filterKeys { k -> k != key })
                .put(key, valueJson)
                .build()
            v.copy(system_attributes = attrs)
        }
    }

}