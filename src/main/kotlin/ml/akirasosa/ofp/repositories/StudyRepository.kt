package ml.akirasosa.ofp.repositories

import ml.akirasosa.ofp.StudyDirection
import ml.akirasosa.ofp.domain.Study

interface StudyRepository : FirestoreRepository {
    suspend fun findById(studyId: Long): Study?
    suspend fun findByName(studyName: String): Study?
    suspend fun findAll(): List<Study>
    suspend fun create(studyName: String): Study
    suspend fun setStudyUserAttr(studyId: Long, key: String, valueJson: String)
    suspend fun setStudyDirection(studyId: Long, direction: StudyDirection)
    suspend fun setStudySystemAttr(studyId: Long, key: String, valueJson: String)
}