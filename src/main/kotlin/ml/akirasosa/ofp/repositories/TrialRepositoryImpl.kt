package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.*
import com.google.cloud.firestore.DocumentChange.Type.ADDED
import com.google.cloud.firestore.DocumentChange.Type.MODIFIED
import com.google.common.collect.ImmutableMap
import com.google.firebase.database.utilities.encoding.CustomClassMapper
import io.reactivex.Observable
import ml.akirasosa.ofp.TrialState
import ml.akirasosa.ofp.domain.Trial
import org.slf4j.LoggerFactory

class TrialRepositoryImpl(
    db: Firestore,
    private val sequenceRepository: SequenceRepository,
    path: String = "optuna_trials"
) : TrialRepository {

    private val logger = LoggerFactory.getLogger(TrialRepositoryImpl::class.java.name)
    private val collection: CollectionReference = db.collection(path)

    override suspend fun create(trial: Trial): Trial {
        val newTrialId = sequenceRepository.getSequenceOf(collection.id)
        val newTrial = trial.copy(trial_id = newTrialId)

        @Suppress("UNCHECKED_CAST")
        val converted = CustomClassMapper.convertToPlainJavaTypes(newTrial) as HashMap<String, Any>
        converted["datetime_start"] = FieldValue.serverTimestamp()
        document(newTrialId).set(converted).await()

        return newTrial
    }

    override suspend fun findAllByStudyId(studyId: Long): List<Trial> {
        return collection.whereEqualTo("study_id", studyId)
            .get()
            .await()
            .toObjects(Trial::class.java)
    }

    override suspend fun findAll(): List<Trial> {
        return collection.get().await().toObjects(Trial::class.java)
    }

    override fun getTrialsObservables(studyId: Long): Observable<Trial> {
        return Observable.create<Trial> {
            val listenerRegistration = collection
                .whereEqualTo("study_id", studyId)
                .addSnapshotListener(EventListener<QuerySnapshot> { snapshots, e ->
                    if (e != null) {
                        logger.error("Something bad happened in getting realtime update", e)
                        it.onError(e)
                        return@EventListener
                    }

                    snapshots?.documentChanges?.forEach { dc ->
                        val trial = dc.document.toObject(Trial::class.java)
                        when (dc.type) {
                            ADDED -> it.onNext(trial)
                            MODIFIED -> it.onNext(trial)
                            else -> {
                            }
                        }
                    }
                })
            it.setCancellable { listenerRegistration.remove() }
        }
    }

    override suspend fun setTrialState(trialId: Long, state: TrialState): WriteResult {
        val params = ImmutableMap.builder<String, Any>()
            .put("state", state.toString())
            .apply {
                if (state.isFinished()) {
                    put("datetime_complete", FieldValue.serverTimestamp())
                }
            }
            .build()

        return document(trialId)
            .set(params, SetOptions.merge())
            .await()
    }

    override suspend fun setTrialValue(trialId: Long, value: Double): WriteResult {
        val params = mapOf("value" to value)

        return document(trialId)
            .set(params, SetOptions.merge())
            .await()
    }

    override suspend fun setTrialParam(
        trialId: Long,
        paramName: String,
        paramValue: Double,
        distributionJson: String
    ): WriteResult {
        val params = mapOf(
            "trial_params" to mapOf(
                paramName to mapOf(
                    "param_value" to paramValue,
                    "distribution_json" to distributionJson
                )
            )
        )

        return document(trialId)
            .set(params, SetOptions.merge())
            .await()
    }

    override suspend fun setTrialIntermediateValue(
        trialId: Long,
        step: Int,
        intermediateValue: Double
    ): WriteResult {
        val params = mapOf(
            "trial_values" to mapOf(
                // Firestore supports only string key
                step.toString() to intermediateValue
            )
        )

        return document(trialId)
            .set(params, SetOptions.merge())
            .await()
    }

    override suspend fun setStudyUserAttr(trialId: Long, key: String, valueJson: String): WriteResult {
        val params = mapOf(
            "user_attributes" to mapOf(
                key to valueJson
            )
        )

        return document(trialId)
            .set(params, SetOptions.merge())
            .await()
    }

    override suspend fun setStudySystemAttr(trialId: Long, key: String, valueJson: String): WriteResult {
        val params = mapOf(
            "system_attributes" to mapOf(
                key to valueJson
            )
        )

        return document(trialId)
            .set(params, SetOptions.merge())
            .await()
    }

    private fun document(trialId: Long) = collection.document(trialId.toString())

    private fun TrialState.isFinished(): Boolean {
        return this == TrialState.COMPLETE || this == TrialState.PRUNED
    }

}

