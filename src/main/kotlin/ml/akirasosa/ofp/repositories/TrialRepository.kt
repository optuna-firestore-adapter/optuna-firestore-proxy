package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.WriteResult
import io.reactivex.Observable
import ml.akirasosa.ofp.TrialState
import ml.akirasosa.ofp.domain.Trial

interface TrialRepository : FirestoreRepository {
    suspend fun create(trial: Trial): Trial
    suspend fun findAllByStudyId(studyId: Long): List<Trial>
    suspend fun findAll(): List<Trial>
    suspend fun setTrialState(trialId: Long, state: TrialState): WriteResult
    suspend fun setTrialValue(trialId: Long, value: Double): WriteResult
    suspend fun setTrialParam(
        trialId: Long,
        paramName: String,
        paramValue: Double,
        distributionJson: String
    ): WriteResult

    suspend fun setTrialIntermediateValue(trialId: Long, step: Int, intermediateValue: Double): WriteResult
    suspend fun setStudyUserAttr(trialId: Long, key: String, valueJson: String): WriteResult
    suspend fun setStudySystemAttr(trialId: Long, key: String, valueJson: String): WriteResult
    fun getTrialsObservables(studyId: Long): Observable<Trial>
}