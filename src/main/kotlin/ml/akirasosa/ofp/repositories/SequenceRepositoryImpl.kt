package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.Firestore


class SequenceRepositoryImpl(
    private val db: Firestore,
    path: String = "optuna_seq"
) : SequenceRepository {

    private val collection = db.collection(path)

    override suspend fun getSequenceOf(name: String): Long {
        val docRef = collection.document(name)

        val transaction = db.runTransaction { tx ->
            val snapshot = tx.get(docRef).get()
            val oldSeq = snapshot.getLong("seq") ?: 0
            oldSeq.plus(1).also {
                tx.set(docRef, mapOf("seq" to it))
            }
        }

        return transaction.await()
    }

}