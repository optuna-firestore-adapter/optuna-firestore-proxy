package ml.akirasosa.ofp.repositories

import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.io.FileInputStream
import com.google.cloud.firestore.FirestoreOptions
import ml.akirasosa.ofp.domain.Trial
import java.lang.NullPointerException
import java.util.concurrent.ConcurrentHashMap


val repositoryModule = Kodein.Module("repositoryModule") {

    bind<Firestore>() with singleton {
        val app = FirebaseApp.getApps()
            .firstOrNull { it.name == FirebaseApp.DEFAULT_APP_NAME }

        if (app == null) {
            FirebaseOptions.Builder()
                .setCredentials(getCredentials())
                .build()
                .let { FirebaseApp.initializeApp(it) }
        }

        FirestoreOptions.newBuilder()
            .setTimestampsInSnapshotsEnabled(true)
            .build()
            .service
    }

    bind<TrialStore>() with singleton {
        val cache1 = ConcurrentHashMap<Long, Trial>()
        val cache2 = ConcurrentHashMap<Long, Trial>()
        Pair(cache1, cache2)
    }

    bind<SequenceRepository>() with provider { SequenceRepositoryImpl(instance()) }
    bind<StudyRepository>() with provider { StudyRepositoryImpl(instance(), instance()) }
    bind<TrialRepository>() with provider { TrialRepositoryImpl(instance(), instance()) }
    bind<TrialCacheRepository>() with provider { TrialCacheRepositoryImpl(instance()) }

}
typealias TrialStore = Pair<ConcurrentHashMap<Long, Trial>, ConcurrentHashMap<Long, Trial>>

private fun getCredentials(): GoogleCredentials {
    return try {
        val credentialPath = System.getenv("GOOGLE_APPLICATION_CREDENTIALS")
        val serviceAccount = FileInputStream(credentialPath)
        GoogleCredentials.fromStream(serviceAccount)
    } catch (e: NullPointerException) {
        println("GOOGLE_APPLICATION_CREDENTIALS was not found. Fallback to getApplicationDefault.")
        GoogleCredentials.getApplicationDefault()
    }
}
