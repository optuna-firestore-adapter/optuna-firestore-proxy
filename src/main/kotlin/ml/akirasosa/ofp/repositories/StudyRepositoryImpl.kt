package ml.akirasosa.ofp.repositories

import com.google.cloud.firestore.CollectionReference
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.SetOptions
import ml.akirasosa.ofp.StudyDirection
import ml.akirasosa.ofp.domain.Study
import java.util.*


class StudyRepositoryImpl(
    db: Firestore,
    private val sequenceRepository: SequenceRepository,
    path: String = "optuna_studies"
) : StudyRepository {

    private val collection: CollectionReference = db.collection(path)

    override suspend fun create(studyName: String): Study {
        val newStudyId = sequenceRepository.getSequenceOf(collection.id)
        val newStudyName = studyName.ifBlank { UUID.randomUUID().toString() }
        val newStudy = Study(study_id = newStudyId, study_name = newStudyName)
        document(newStudyId).set(newStudy).await()

        return newStudy
    }

    override suspend fun findById(studyId: Long): Study? {
        return document(studyId).get().await().toObject(Study::class.java)
    }

    override suspend fun findByName(studyName: String): Study? {
        val future = collection.whereEqualTo("study_name", studyName).get()
        val snapshot = future.await()
        if (snapshot.isEmpty) return null

        return snapshot.documents.first().toObject(Study::class.java)
    }

    override suspend fun findAll(): List<Study> {
        return collection.get().await().toObjects(Study::class.java)
    }

    override suspend fun setStudyUserAttr(studyId: Long, key: String, valueJson: String) {
        val params = mapOf(
            "user_attributes" to mapOf(
                key to valueJson
            )
        )
        document(studyId).set(params, SetOptions.merge()).await()
    }

    override suspend fun setStudyDirection(studyId: Long, direction: StudyDirection) {
        val params = mapOf(
            "direction" to direction.toString()
        )
        document(studyId).set(params, SetOptions.merge()).await()
    }

    override suspend fun setStudySystemAttr(studyId: Long, key: String, valueJson: String) {
        val params = mapOf(
            "system_attributes" to mapOf(
                key to valueJson
            )
        )
        document(studyId).set(params, SetOptions.merge()).await()
    }

    private fun document(id: Long) = collection.document(id.toString())

}

