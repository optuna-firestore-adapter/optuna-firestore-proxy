# Optuna Firestore Proxy

[![pipeline status](https://gitlab.com/optuna-firestore-adapter/optuna-firestore-proxy/badges/master/pipeline.svg)](https://gitlab.com/optuna-firestore-adapter/optuna-firestore-proxy/commits/master)


The Optuna Firestore Proxy allows Optuna user to connect with Firestore. It works by opening gRPC connection with Python client adapter. While it's running, it keeps observing realtime updates from Firestore. Trial data will be cached, so that its network latency is essentially same with in memory DB.

See more on [Python storage adapter project](https://gitlab.com/optuna-firestore-adapter/optuna-firestore-storage).
